from django.contrib import admin
from .models import AutomobileVO, SalesCustomer, SalesPerson, Sale

admin.site.register(AutomobileVO)
admin.site.register(SalesCustomer)
admin.site.register(SalesPerson)
admin.site.register(Sale)
