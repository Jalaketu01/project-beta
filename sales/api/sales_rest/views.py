from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, SalesCustomer, Sale
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "import_href",
        "id"
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number"
    ]


class SalesCustomerEncoder(ModelEncoder):
    model = SalesCustomer
    properties = [
        "name",
        "address",
        "phone"
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobileVO",
        "sales_person",
        "sales_customer"
        ]
    encoders = {
        "automobileVO": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "sales_customer": SalesCustomerEncoder()
        }


@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_salescustomer(request):
    if request.method == "GET":
        salescustomers = SalesCustomer.objects.all()
        return JsonResponse(
            {"salescustomers": salescustomers},
            encoder=SalesCustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salescustomer = SalesCustomer.objects.create(**content)
            return JsonResponse(
                salescustomer,
                encoder=SalesCustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salescustomer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        content["automobileVO"] = AutomobileVO.objects.get(vin=content["automobileVO"])
        content["sales_person"] = SalesPerson.objects.get(name=content["sales_person"])
        content["sales_customer"] = SalesCustomer.objects.get(name=content["sales_customer"])
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
