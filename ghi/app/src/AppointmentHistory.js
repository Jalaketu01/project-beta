import React from 'react';

class AppointmentHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: "",
            appointments: [],
        };
        this.handleVinChange = this.handleVinChange.bind(this);
        this.vinSearch = this.vinSearch.bind(this);
    }

    async vinSearch(event) {
        event.preventDefault();
        const data = { ...this.state };
        const vinUrl = `http://localhost:8080/api/appointments/${data.vin}/`;
        const response = await fetch(vinUrl);
        if (response.ok) {
            const data = await response.json();
            this.setState({ appointments: data })
            const cleared = {
                vin: "",
                appointment: "",
            };
            this.setState(cleared);
        }
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    render() {
        return (
            <>
                <div className="input-group">
                    <form onSubmit={this.vinSearch} id="search-bar" className='search-bar'>
                        <input onChange={this.handleVinChange} value={this.state.vin} required placeholder="vin"
                            type="text" id="vin" name="vin" maxLength="17" className="form-control rounded" />
                        <button className="btn btn-success"> Search!</button>
                    </form>
                </div>

                <div className="search-form">
                    <h1>Appointment History of Selected Vehicle</h1>
                    <table className="table table-dark table-striped">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Reason</th>
                                <th>VIP</th>
                                <th>Assignee</th>

                            </tr>
                        </thead>
                        <tbody>
                            {this.state.appointments.map(appointment => {
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.customer_name}</td>
                                        <td>{appointment.date}</td>
                                        <td>{appointment.time}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.vip && <button className="btn btn-warning"></button>}</td>
                                        <td>{appointment.technician.tech_name}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </>
        );
    }
}
export default AppointmentHistory;
