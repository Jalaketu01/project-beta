import React from 'react';

// Try to use the snake_case directly this time
class TechnicianForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tech_name: "",
            tech_id: "",
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleIDChange = this.handleIDChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ tech_name: value })
    }

    handleIDChange(event) {
        const value = event.target.value;
        this.setState({ tech_id: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };

        const techUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(techUrl, fetchConfig);
        if (response.ok) {
            const newTech = await response.json();

            this.setState({
                tech_name: "",
                tech_id: ""
            });
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Join the Team!</h1>
                        <form onSubmit={this.handleSubmit} id="create-tech-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.tech_name} onChange={this.handleNameChange} placeholder="tech_name" required type="text" name="tech_name" id="tech_name" className="form-control" />
                                <label htmlFor="tech_name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.tech_id} onChange={this.handleIDChange} placeholder="tech_id" required type="number" min="1" name="tech_id" id="tech_id" className="form-control" />
                                <label htmlFor="tech_id">Technician ID</label>
                            </div>
                            <button className="btn btn-primary">Join Now!!!</button>
                        </form>
                    </div>
                    <div>
                        <a href='http://localhost:3000/technicians/'><button type='button'>TECHNICIANS</button></a>
                    </div>
                </div>
            </div>
        );
    }
}

export default TechnicianForm;
