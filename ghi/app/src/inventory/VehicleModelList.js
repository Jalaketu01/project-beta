import React from 'react';
import { useEffect, useState } from "react";


const VehicleModelList = () => {
    const [models, setModels] = useState([]);
    // First argument: state variable
    // Second argument: function

    useEffect(() => {
        fetch('http://localhost:8100/api/models/')
            .then(response => response.json())
            .then(data => {
                setModels(data.models);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
        <>
            <h1>Our Models</h1>
            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{model.manufacturer.name}</td>
                                <td>{model.name}</td>
                                <td><img src={model.picture_url} alt={model.name} width="20%" height="20%" /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div>
                <a href='http://localhost:3000/vehiclemodels/new/'><button type='button'>NEW MODEL</button></a>
            </div>
        </>
    );
}

export default VehicleModelList;
