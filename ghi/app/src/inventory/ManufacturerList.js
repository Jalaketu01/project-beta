import React from 'react';
import { useEffect, useState } from "react";


const ManufacturerList = () => {
    const [manufacturers, setManufacturers] = useState([]);


    useEffect(() => {
        fetch('http://localhost:8100/api/manufacturers/')
            .then(response => response.json())
            .then(data => {
                setManufacturers(data.manufacturers);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
        <>
            <h1>List of Manufacturers</h1>
            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div>
                <a href='http://localhost:3000/manufacturers/new/'><button type='button'>NEW MANUFACTURER</button></a>
            </div>
        </>
    );
}

export default ManufacturerList;
