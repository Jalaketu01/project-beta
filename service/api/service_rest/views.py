from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment  # May need to edit later
import json


# Encoder template
class AutomobileVO(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href"]

    def get_extra_data(self, o):
        return {"id": o.id}


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["tech_name", "tech_id"]

    # def get_extra_data(self, o):
    #     return {"id": o.id}


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "date",
        "time",
    ]

    def get_extra_data(self, o):
        return {
            "technician": o.technician.tech_name,
            "reason": o.reason,
            "vip": o.vip,
            "finished": o.finished,
            "id": o.id,
            "vin": o.vin,
        }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "date",
        "time",
        "reason",
        "vip",
        "finished",
        "id",
        "technician",
    ]

    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods("GET")
def api_detail_technician(request, pk):
    if request.method == "GET":
        technicians = Technician.objects.get(id=pk)
        return JsonResponse(
            technicians,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all().order_by(
            "-date"
        )  # order bt most recent
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        print(content, content["technician"])
        try:

            technician = Technician.objects.get(tech_id=content["technician"])
            print(technician)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician"},
                status=400,
            )
        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_appointment(request, pk):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointments,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response

    elif request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointments = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})


@require_http_methods(["GET"])
def api_vin_appointments(request, vin):
    appointments = Appointment.objects.filter(vin=vin)
    if appointments:
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        response = JsonResponse({"message": "No History found"})
        response.status_code = 400
        return response
